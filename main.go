package main

import (
	"master/config"
	"master/controllers"

	"github.com/subosito/gotenv"
)

func init() {
	gotenv.Load()
}

func main() {

	db := config.ConnectDatabase()
	defer db.Close()

	controllers.CreatePersonController(db)
}
