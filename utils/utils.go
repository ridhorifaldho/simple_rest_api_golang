package utils

import (
	"master/model"
	"net/http"

	"github.com/gin-gonic/gin"
)

// ResponseError ...
func ResponseError(c *gin.Context, status int, msg string) *gin.Context {
	c.JSON(status, gin.H{
		"status":  "0000",
		"message": msg,
	})

	return c
}

// ResponseSuccess ...
func ResponseSuccess(c *gin.Context, status int, data interface{}) *gin.Context {
	c.JSON(status, gin.H{
		"Status":  "0000",
		"Message": "Success",
		"data":    data,
	})

	return c
}

// ValidatePerson ...
func ValidatePerson(c *gin.Context, person *model.Person) bool {
	if person.FirstName == "" {
		ResponseError(c, http.StatusBadRequest, "Firstname Cannot be empty")
		return false
	}

	if person.LastName == "" {
		ResponseError(c, http.StatusBadRequest, "Lastname cannot be empty")
		return false
	}

	return true
}
