package controllers

import (
	"fmt"
	"master/model"
	"master/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// code your controllers to database here...

// PersonController ...
type PersonController struct {
	DB *gorm.DB
}

// CreatePersonController ...
func CreatePersonController(db *gorm.DB) {
	personController := PersonController{db}
	router := gin.New()

	// person := router.Group("/api")

	router.GET("/user/getid/:id", personController.GetPerson)
	router.POST("/user/create", personController.CreatePerson)
	router.GET("/user/getall", personController.GetAllPerson)
	router.DELETE("/user/delete/:id", personController.DeletePerson)
	router.PATCH("/user/update", personController.UpdatePerson)

	router.Run(":9094")

}

// CreatePerson ...
func (p *PersonController) CreatePerson(c *gin.Context) {
	var person model.Person

	err := c.ShouldBindJSON(&person)
	if err != nil {
		fmt.Println("[PersonController.createPerson] Error when decoder data from body")
		utils.ResponseError(c, http.StatusInternalServerError, "Oppss, something error")
		return
	}

	isValid := utils.ValidatePerson(c, &person)
	if !isValid {
		return
	}

	errs := p.DB.Debug().Create(&person).Error
	if errs != nil {
		fmt.Println("[PersonController.createPerson] Error when query save data")
		utils.ResponseError(c, http.StatusInternalServerError, "Oppss, something error")
		return
	}

	utils.ResponseSuccess(c, http.StatusOK, person)

}

// GetAllPerson ...
func (p *PersonController) GetAllPerson(c *gin.Context) {
	var persons []model.Person

	err := p.DB.Debug().Find(&persons).Error
	if err != nil {
		fmt.Println("[PersonController.GetAllPerson] Error when query get all data")
		utils.ResponseError(c, http.StatusInternalServerError, "Oppss, something error")
		return
	}

	utils.ResponseSuccess(c, http.StatusOK, persons)
}

// GetPerson ...
func (p *PersonController) GetPerson(c *gin.Context) {
	var person model.Person
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	fmt.Println(id)
	if err != nil {
		fmt.Println("[PersonController.GetPerson] Error when convert param")
		utils.ResponseError(c, http.StatusInternalServerError, "ID not valid !!")
		return
	}

	err = p.DB.Debug().Where("id = ?", id).First(&person).Error
	fmt.Println(person)
	if err != nil {
		fmt.Println("[PersonController.GetAllPerson] Error when query get data")
		utils.ResponseError(c, http.StatusInternalServerError, "Oppss, something error")
		return
	}

	utils.ResponseSuccess(c, http.StatusOK, person)

}

// UpdatePerson ...
func (p *PersonController) UpdatePerson(c *gin.Context) {
	var person model.Person
	var tempPerson model.Person

	err := c.ShouldBindJSON(&person)

	err = p.DB.Debug().Where("id = ?", person.ID).First(&tempPerson).Error
	if err != nil {
		fmt.Println("[PersonController.UpdatePerson] Error when query get data")
		utils.ResponseError(c, http.StatusInternalServerError, "personID does not exist")
		return
	}

	if person.FirstName == "" {
		person.FirstName = tempPerson.FirstName
	}

	if person.LastName == "" {
		person.LastName = tempPerson.LastName
	}

	err = p.DB.Debug().Model(&person).Where("id = ?", tempPerson.ID).Update(person).Error
	if err != nil {
		utils.ResponseError(c, http.StatusInternalServerError, "Failed to update data")
		fmt.Printf("[PersonController.UpdatePerson] Error when request data to usecase with error : %v", err)
		return
	}

	utils.ResponseSuccess(c, http.StatusOK, person)
}

// DeletePerson ...
func (p *PersonController) DeletePerson(c *gin.Context) {
	var person model.Person

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		fmt.Println("[PersonController.DeletePerson] Error when convert param")
		utils.ResponseError(c, http.StatusInternalServerError, "ID not valid !!")
		return
	}

	err = p.DB.Debug().Where("id = ? ", id).Delete(&person).Error
	if err != nil {
		fmt.Println("[PersonController.DeletePerson] Error when query data")
		utils.ResponseError(c, http.StatusInternalServerError, "Oppss, something error")
		return
	}

	utils.ResponseSuccess(c, http.StatusOK, "Success delete data")
}
