package config

import (
	"fmt"
	"log"
	"master/model"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // import library postgres
)

// config your database here..

// ConnectDatabase ...
func ConnectDatabase() *gorm.DB {
	con := os.Getenv("URL_DATABASE")
	db, err := gorm.Open("postgres", con)
	if err != nil {
		fmt.Println("Failed to connect database")
		log.Fatal()
	}

	InitTable(db)
	fmt.Println("Success connect to database")
	return db
}

// InitTable ...
func InitTable(db *gorm.DB) {
	db.Debug().AutoMigrate(
		&model.Person{},
	)
}
