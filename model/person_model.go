package model

// Person ...
type Person struct {
	ID        int    `gorm:"PRIMARY_KEY" json:"id"`
	FirstName string `gorm:"type:varchar(50);NOT NULL;DEFAULT:'0'" json:"first_name" `
	LastName  string `gorm:"type:varchar(50);NOT NULL;DEFAULT:'0'" json:"last_name" `
}

// TableName ...
func (u Person) TableName() string {
	return "tb_person"
}
